<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class StartDepositWorks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deposit:run {login}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run deposit';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $user = User::where('login', $this->argument('login'))->first();

        for ($i = 0; $i < 10; $i++) {
            (new \App\Http\Controllers\DepositController)->deposit($user);
            sleep(60);
        }
    }
}
