<?php

namespace App\DTO;

abstract class AbstractDTO
{
    /**
     * @param array $data
     * @param array $propsMap - list, where key - $data key, value - dto class property name
     */
    public function __construct(array $data = [], array $propsMap = [])
    {
        foreach ($data as $property => $value) {
            $property = $propsMap[$property] ?? $property;

            if (property_exists($this, $property)) {
                $this->$property = $value;
            }
        }
    }

    public function isInitialized(string $property): bool
    {
        $rp = new \ReflectionProperty($this, $property);
        return $rp->isInitialized($this);
    }

    public function toArray(): array
    {
        return (array)$this;
    }
}
