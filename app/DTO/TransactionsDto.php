<?php

namespace App\DTO;

use App\Models\User;

class TransactionsDto extends AbstractDTO
{
    public User $user;
    public ?int $walletId = null;
    public ?int $depositId = null;
    public ?int $amount = null;
    public ?string $type = null;
}
