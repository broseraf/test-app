<?php

namespace App\Http\Controllers;


use App\DTO\TransactionsDto;
use App\Http\Requests\DepositRefillRequest;
use App\Models\Deposit;
use App\Models\User;
use App\Models\Wallet;
use App\Services\TransactionsInterface;
use App\Services\TransactionsService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DepositController extends Controller
{
    public function createDeposit(DepositRefillRequest $request, TransactionsInterface $transactions): RedirectResponse
    {
        $amount = $request->validated();
        DB::transaction(function() use ($amount, $transactions) {
            $user = Auth::user();
            $deposit = Deposit::where('user_id', $user->id)->first();
            $wallet = Wallet::where('user_id', $user->id)->first();
            if (!$deposit) {
                $data['user_id'] = $user->id;
                $data['wallet_id'] = $user->Wallet->id;
                $data['active'] = 1;
                $data['percent'] = 20;
                $deposit = Deposit::create($data);
            }
            if ($amount['amount'] <= $wallet->balance) {
                $wallet->update(['balance' => $wallet->balance - $amount['amount']]);

                $dto = new TransactionsDto();
                $dto->user = $user;
                $dto->walletId = $wallet->id;
                $dto->depositId = $deposit->id;
                $dto->amount = $amount['amount'];
                $dto->type = "create_deposit";
                $transactions::createTransactions($dto);

                $deposit->update(['invested' => $deposit['invested'] + $amount['amount']]);
            }
            if ($deposit->invested < 10 || $deposit->invested > 100) {
                DB::rollBack();
            }
        });
        return redirect('/home');
    }

    /**
     *
     */
    public function runCommand()
    {
        Artisan::call('deposit:run ' . Auth::user()->login);
    }

    public function deposit(User $user)
    {
        $transactionsService = new TransactionsService();

        if ($user->Deposit->active) {
            self::accrue($user, $transactionsService);
        }
    }

    public function accrue($user, TransactionsInterface $transactions)
    {
        DB::transaction(function() use ($user, $transactions) {
            $deposit = Deposit::where('user_id', $user->id)->first();
            $wallet = Wallet::where('user_id', $user->id)->first();

            if ($deposit->accrue_times < 9) {
                $total = ($deposit->invested * $deposit->percent) / 100;
                $wallet->update(['balance' => $wallet->balance + $total]);
                $deposit->update(['accrue_times' => $deposit->accrue_times + 1, 'duration' => $deposit->duration + $total,]);

                $dto = new TransactionsDto();
                $dto->user = $user;
                $dto->walletId = $wallet->id;
                $dto->depositId = $deposit->id;
                $dto->amount = $total;
                $dto->type = "accrue";

                $transactions::createTransactions($dto);

                if ($deposit->accrue_times >= 9) {
                    $deposit->update(['accrue_times' => $deposit->accrue_times + 1, 'duration' => $deposit->duration + $total]);

                    $dto = new TransactionsDto();
                    $dto->user = $user;
                    $dto->walletId = $wallet->id;
                    $dto->depositId = $deposit->id;
                    $dto->amount = $total;
                    $dto->type = "close_deposit";

                    $transactions::createTransactions($dto);
                    $deposit->update(['active' => false]);
                }
            }
        });
    }
}
