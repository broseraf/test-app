<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home()
    {
        $user = Auth::user();

        return view('home', ["user" => $user]);
    }

    public function index()
    {
        $user = Auth::user();
        if (isset($user->Deposit)) {
            return view('welcome', ["deposit" => $user->Deposit, "transactions" => $user->Transaction]);
        }
        return view('welcome');
    }
}
