<?php

namespace App\Http\Controllers;


use App\DTO\TransactionsDto;
use App\Http\Requests\WalletRefillRequest;
use App\Models\Wallet;
use App\Services\TransactionsInterface;
use App\Services\TransactionsService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class WalletController extends Controller
{
    public function enter(WalletRefillRequest $request, TransactionsInterface $transactions)
    {
        $amount = $request->validated();
        DB::transaction(function() use ($amount, $transactions) {
            $user = Auth::user();
            $wallet = Wallet::where('user_id', $user->id)->first();
            $wallet->update(['balance' => $wallet->balance + $amount['amount']]);


            $dto = new TransactionsDto();
            $dto->user = $user;
            $dto->walletId = $wallet->id;
            $dto->amount = $amount['amount'];
            $dto->type = "enter";

            $transactions::createTransactions($dto);
        });
        return redirect('/home');
    }


}
