<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Deposit
 *
 * @property int $id
 * @property int $user_id
 * @property int $wallet_id
 * @property float $invested
 * @property float $percent
 * @property int $active
 * @property int $duration
 * @property int $accrue_times
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User|null $User
 * @property-read \App\Models\Wallet|null $Wallet
 * @method static \Illuminate\Database\Eloquent\Builder|Deposit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Deposit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Deposit query()
 * @method static \Illuminate\Database\Eloquent\Builder|Deposit whereAccrueTimes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deposit whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deposit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deposit whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deposit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deposit whereInvested($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deposit wherePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deposit whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deposit whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deposit whereWalletId($value)
 * @mixin \Eloquent
 */
class Deposit extends Model
{
    protected $fillable = [
        'user_id',
        'wallet_id',
        'invested',
        'percent',
        'active',
        'duration',
        'accrue_times',
    ];

    public function User()
    {
        return $this->hasOne(User::class, 'user_id', 'id');
    }

    public function Wallet()
    {
        return $this->hasOne(Wallet::class, 'wallet_id	', 'id');
    }
}
