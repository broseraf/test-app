<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Transactions
 *
 * @property int $id
 * @property string $type
 * @property int $user_id
 * @property int $wallet_id
 * @property int|null $deposit_id
 * @property float $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Deposit|null $Deposit
 * @property-read \App\Models\User|null $User
 * @property-read \App\Models\Wallet|null $Wallet
 * @method static \Illuminate\Database\Eloquent\Builder|Transactions newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Transactions newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Transactions query()
 * @method static \Illuminate\Database\Eloquent\Builder|Transactions whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transactions whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transactions whereDepositId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transactions whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transactions whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transactions whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transactions whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transactions whereWalletId($value)
 * @mixin \Eloquent
 */
class Transactions extends Model
{
    protected $fillable = [
        'type',
        'user_id',
        'wallet_id',
        'deposit_id',
        'amount',
    ];

    public function User()
    {
        return $this->hasOne(User::class, 'user_id', 'id');
    }

    public function Wallet()
    {
        return $this->hasOne(Wallet::class, 'wallet_id', 'id');
    }

    public function Deposit()
    {
        return $this->hasOne(Deposit::class, 'deposit_id', 'id');
    }
}
