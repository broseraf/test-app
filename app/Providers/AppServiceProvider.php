<?php

namespace App\Providers;

use App\Services\Employer\EmployerService;
use App\Services\Employer\EmployerServiceInterface;
use App\Services\TransactionsInterface;
use App\Services\TransactionsService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerServices();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function registerServices() {
        $this->app->singleton(
            TransactionsInterface::class,
            TransactionsService::class
        );
    }
}
