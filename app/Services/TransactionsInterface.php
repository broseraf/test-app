<?php

namespace App\Services;

use App\DTO\TransactionsDto;

interface TransactionsInterface
{
    public static function createTransactions(TransactionsDto $dto): void;
}
