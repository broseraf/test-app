<?php
/**
 * Created by PhpStorm.
 * User: BroSeraf
 * Date: 12/16/2020
 * Time: 6:44 PM
 */

namespace App\Services;



use App\DTO\TransactionsDto;
use App\Models\Transactions;

class TransactionsService implements TransactionsInterface
{
    public static function createTransactions(TransactionsDto $dto): void
    {
        $transaction = new Transactions();
        $transaction->user_id = $dto->user->id;
        $transaction->wallet_id = $dto->walletId;
        $transaction->deposit_id = $dto->depositId;
        $transaction->type = $dto->type;
        $transaction->amount = $dto->amount;
        $transaction->save();
    }
}
