@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="form-group row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">{{ __('Balance') }}</div>
                    <div class="card-body">
                        <p>{{$user->Wallet->balance}}</p>
                    </div>

                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">{{ __('Deposit') }}</div>
                    <div class="card-body">
                        <p>@if($user->Deposit) {{$user->Deposit->invested }} @else 0 @endif</p>
                    </div>

                </div>
            </div>
        </div>
        <div class="form-group row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Wallet</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('enter') }}">
                            @csrf
                            <div class="form-group">
                                <label for="amount">Amount</label>
                                <input type="number" class="form-control" name="amount" required id="amount"
                                       placeholder="Enter amount">
                            </div>
                            <button type="submit" class="btn btn-primary">Add</button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Deposit</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('create_deposit') }}">
                            @csrf
                            <div class="form-group">
                                <label for="amount">Amount</label>
                                <input type="number" class="form-control" name="amount" required id="amount"
                                       placeholder="Enter amount">
                            </div>
                            @if($errors)
                                <div class="text-danger"><small>{{ $errors->first('amount') }}</small></div>
                            @endif
                            <button type="submit" class="btn btn-primary">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
