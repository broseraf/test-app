@extends('layouts.app')

@section('content')
    <div class="container">
        @if(isset($deposit))
            <div class="form-group row">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#Id</th>
                        <th scope="col">Deposit amount</th>
                        <th scope="col">Deposit percentage</th>
                        <th scope="col">Number of charges</th>
                        <th scope="col">Amount of charges</th>
                        <th scope="col">Status</th>
                        <th scope="col">Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{$deposit->id}}</td>
                        <td>{{$deposit->invested}}</td>
                        <td>{{$deposit->percent}}</td>
                        <td>{{$deposit->accrue_times}}</td>
                        <td>{{$deposit->duration}}</td>
                        <td>@if($deposit->active) Active @else Inactive @endif</td>
                        <td>{{$deposit->created_at}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="form-group row">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#Id</th>
                        <th scope="col">Type</th>
                        <th scope="col">Amount</th>
                        <th scope="col">Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($transactions as $transaction)
                        <tr>
                            <td>{{$transaction->id}}</td>
                            <td>{{$transaction->type}}</td>
                            <td>{{$transaction->amount}}</td>
                            <td>{{$transaction->created_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
{{--                <div class="col-md-6">--}}
{{--                    <div class="card">--}}
{{--                        <div class="card-body">--}}
{{--                            <p class="text-center">Home</p>--}}
                            <a href="/home"><p class="text-center">Balance</p></a>
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        @else
            <div class="form-group row justify-content-center">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <p class="text-center">Nothing is found</p>
                            <a href="/home"><p class="text-center">Perhaps you want to make a wallet or deposit</p></a>
                        </div>

                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
