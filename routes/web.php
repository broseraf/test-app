<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'HomeController@index')->name('index');


Route::middleware('auth')->group(function () {

    Route::get('/home', 'HomeController@home')->name('home');
    Route::post('/enter', 'WalletController@enter')->name('enter');
    Route::post('/create_deposit', 'DepositController@createDeposit')->name('create_deposit');
    Route::post('/deposit', 'DepositController@runCommand')->name('deposit');

});